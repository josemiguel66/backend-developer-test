<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('id', 'desc')->get();
        return response()->json([
            'data' => $users,
        ]);
    }

    /**
    * descripción de la rutina
    */
    public function filter(Request $request)
    {
        $search = ($request->search) ? $request->search : '';
        $search = "%$search%";

        $users = User::where('name', 'like', $search)
                        ->orWhere('email', 'like', $search)
                        ->orderBy('name')
                        ->get();

        return response()->json([
            'data' => $users,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $user = User::findOrFail($id);

            $validator = Validator::make($request->all(), [
                'name'     => 'required',
                'email'    => 'required|string|max:100|unique:users,email,'.$user->id,
                'password' => ($request->password) ? 'required|min:8|confirmed' : '',
            ],[
                'email.required'     => 'El email es requerido',
                'email.email'        => 'El email no tiene el formato requerido',
                'email.unique'       => 'El email indicado ya se encuentra registrado',
                'email.max'          => 'El email no debe ser mayor a 100 caracteres',
                'name.required'      => 'El nombre es requerido',
                'password.required'  => 'La contraseña es requerida',
                'password.min'       => 'La contraseña debe ser de al menos 8 caracteres',
                'password.confirmed' => 'La contraseña y su confirmación no coinciden',
            ]);

            #Verificar si la validación falló
            if ($validator->fails()) {
                return response()->json([
                    'response' => 'No se ha podido completar la solicitud.',
                    'errors'   => $validator->errors()->all()
                ], 422);
            }

            $user->update([
                'name'     => $request->name,
                'email'    => $request->email,
                'password' => bcrypt($request->password),
            ]);

            return response()->json([
                'response' => 'Usuario actualizado con éxito',
                'data'     => $user,
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'response' => 'No se puede actualizar este usuario.',
            ],404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {

            if ($request->user == auth()->id()){
                abort(404);
            }

            User::findOrFail($id)->delete();
            $users = User::orderBy('id','desc')->get();

            return response()->json([
                'response' => 'Usuario eliminado con éxito',
                'data'     => $users,
            ]);

        } catch (\Exception $ex) {
            return response()->json([
                'response' => 'No se puede eliminar este usuario.',
            ],404);
        }
    }
}
