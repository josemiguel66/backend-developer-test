<?php

namespace App\Http\Controllers;

use App\Mail\RecoverPassword;
use App\Models\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ForgotPasswordController extends Controller
{

    public function sendRecoverEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users',
        ],[
            'email.required' => 'El email es requerido',
            'email.email'    => 'El email no tiene el formato requerido',
            'email.exists'   => 'El email indicado no se encuentra registrado',
        ]);

        #Verificar si la validación falló

        if ($validator->fails()) {
            return response()->json([
                'response' => 'No se ha podido completar la solicitud.',
                'errors'   => $validator->errors()->all()
            ], 422);
        }

        #Crear el token

        $token = Str::random(64);

        DB::table('password_resets')->insert([
                'email'      => $request->email,
                'token'      => $token,
                'created_at' => Carbon::now()
            ]
        );

        $user = User::whereEmail($request->email)->firstOrFail();

        $data = [
            'name'  => $user->name,
            'token' => $token,
        ];

        #Enviar mail

        Mail::to([[
            'name'  => $user->name,
            'email' => $user->email
        ]])->send(new RecoverPassword(compact('data')));

        return response()->json([
            'response' => 'Solicitud de restablecer contraseña enviada con éxito.',
            'data'=> [
                'token' => $token
            ]
        ]);
    }

    public function resetPassword(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email'    => 'required|email|exists:users',
            'password' => 'required|string|min:8|confirmed',
        ],[
            'email.required'     => 'El email es requerido',
            'email.email'        => 'El email no tiene el formato requerido',
            'email.exists'       => 'El email indicado no se encuentra registrado',
            'password.required'  => 'La contraseña es requerida',
            'password.min'       => 'La contraseña debe ser de al menos 8 caracteres',
            'password.confirmed' => 'La contraseña y su confirmación no coinciden',

        ]);

        #Verificar si la validación falló

        if ($validator->fails()) {
            return response()->json([
                'response' => 'No se ha podido completar la solicitud.',
                'errors'   => $validator->errors()->all()
            ], 422);
        }

        #Verificar el token

        $resetPassword = DB::table('password_resets')
                            ->where([
                                'email' => $request->email,
                                'token' => $request->token
                            ])
                            ->first();

        if (!$resetPassword)
            return response()->json(['response'=> 'No se puede procesar su solicitud'], 404);

        #Actualizar password

        User::where('email', $request->email)
            ->update([
                'password' => bcrypt($request->password)
            ]);

        #Eliminar solicitudes de cambio de contraseña

        DB::table('password_resets')->where(['email' => $request->email])->delete();

        return response()->json([
                'response'=> 'Su contraseña se ha cambiado con éxito'
            ]);
    }
}
