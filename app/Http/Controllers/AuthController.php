<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests\AuthRegisterRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('jwt', ['except' => ['login', 'register']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);

        #Validar intento de login

        if (! $token = auth()->attempt($credentials)) {
            return response()->json([
                'response' => 'Usuario o contraseña inválidos'
            ], 401);
        }

        $user = auth()->user();

        #Guardar fecha/hora inicio sesión

        $user->last_login = now();
        $user->save();

        return $this->respondWithToken($token);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json([
            'response' => 'Desconexión exitosa'
        ]);
    }

    /**
    * Registrar usuario
    */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'     => 'required',
            'email'    => 'required|email|unique:users|max:100',
            'password' => 'required|min:8|confirmed',
        ],[
            'email.required'     => 'El email es requerido',
            'email.email'        => 'El email no tiene el formato requerido',
            'email.unique'       => 'El email indicado ya se encuentra registrado',
            'email.max'          => 'El email no debe ser mayor a 100 caracteres',
            'name.required'      => 'El nombre es requerido',
            'password.required'  => 'La contraseña es requerida',
            'password.min'       => 'La contraseña debe ser de al menos 8 caracteres',
            'password.confirmed' => 'La contraseña y su confirmación no coinciden',
        ]);

        #Verificar si la validación falló

        if ($validator->fails()) {
            return response()->json([
                'response' => 'No se ha podido completar la solicitud.',
                'errors'   => $validator->errors()->all()
            ], 422);
        }

        #Crear usuario

        $user = new User(request()->all());
        $user->password = bcrypt($user->password);
        $user->save();

        return response()->json([
            'response' => 'Usuario creado con éxito',
            'data'     => $user
        ]);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'response' => 'Sesión iniciada',
            'data'     => [
                'token_type'   => 'bearer',
                'access_token' => $token,
                'expires_in'   => auth()->factory()->getTTL() * 60
            ]
        ]);
    }

}
