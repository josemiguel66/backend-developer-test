@component('mail::message')
# Solicitud de recuperación de contraseña

Hola {{ $data['name'] }}.

Hemos recibido una solicitud para restablecer su contraseña.

@component('mail::button', ['url' => route('reset_password', $data['token'])])
Cambiar contraseña
@endcomponent

Saludos.
{{ config('app.name') }}
@endcomponent
