# BackEnd Developer Test


---
Coleción de Postman
---

End-points.postman_collection.json



---
End points sin verificación de token
---

Registrar usuarios  post 	127.0.0.1:8000/api/auth/register
                            campos:
                                name
                                email
                                password
                                password_confirmation

Iniciar sesión      post 	127.0.0.1:8000/api/auth/login
                            campos:
                                email
                                password

---
End points con verificación de token
---

Cerrar sesión       post 	127.0.0.1:8000/api/auth/logout

Listar usuarios     get 	127.0.0.1:8000/api/users

Buscar usuarios     post 	127.0.0.1:8000/api/users/search
                        campos:
                            search

Actualizar usuario  post    127.0.0.1:8000/api/users/{id}
                            campos:
                                _method: put
                                name
                                email
                                (password ?) => opcional
                                (password_confirmation ?) => opcional

Borrar usuario      delete 	127.0.0.1:8000/api/users/{id}

---
Se creó un Job `InactiveUsers` que se ejecutará todos los días a las 00:00 para verificar los usuarios inactivos
---
