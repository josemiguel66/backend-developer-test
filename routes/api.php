<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ForgotPasswordController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::group([

    'prefix' => 'auth'

], function () {
    Route::post('register', [AuthController::class, 'register']);
    Route::post('login',    [AuthController::class, 'login']);

    Route::post('logout',   [AuthController::class, 'logout'])->middleware(['jwt']);

});

Route::middleware('jwt')->group(function() {

    Route::resource('users',        UserController::class);
    Route::post('users/search',     [UserController::class, 'filter']);

});

Route::post('/forgot-password', [ForgotPasswordController::class, 'sendRecoverEmail']);
Route::post('/reset-password',  [ForgotPasswordController::class, 'resetPassword'])->name('reset_password');
